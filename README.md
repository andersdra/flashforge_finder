Cura settings for Flashforge Finder  
Start and end G-code was copied from a FlashPrint '.gx' then modified to add pre extrusion along bed side and also changing end code 'G162 Z' to 'G1 Z140 F420' as bed did not return back down after print  
FlashPrint default slicer settings in 'default.conf'  
Tested on firmware 'finder_1.5.20170419.bin'

XYZ = 140mm  
Origin at center [X]  

X min: -37  
Y min: -55  
X max: 37  
Y max: 55  
Gantry height: 28  

0.4mm nozzle size  
1.75mm material diameter  

[Source of printhead settings.](https://labs.tomasino.org/flashforge-finder-with-cura/) Thanks!  

Start G-code:  

M118 X47.40 Y31.00 Z13.90 T0 ; echo serial?  
M140 S0 T0 ; bed temperature  
M104 S{material_print_temperature} T0 ; nozzle temp  
M107 ; disable fan  
G90 ; absoloute positioning  
G28 ; home  
M132 X Y Z A B ; load home from eeprom  
G1 Z140 F420 ; z to bottom  
G161 X Y F3300 ; home to minimum  
M6 T0 ; tool change  
M651 ; fan on  
M907 X100 Y100 Z40 A80 B20 ; set motor current  
G1 X42.48 Y24.82 F4800 ; move  
M108 T0 ; cancel heating  
G92 E0 ; zero extruded  
G1 X75 Y-70 Z[first_layer_height] ; move nozzle to bed  
G1 X75 Y70 E15 F1600.000 ; print line  
G92 E0 ; zero again  
M108 T0 ; cancel heating  
M106 ; fan on  

End:  

M104 S0 T0 ; set nozzle to 0 degrees  
G1 Z140 F420 ; bed to bottom  
G28 X Y ; move to origin  
M132 X Y A B ; pid d value  
M652 ; fan off  
G91; relative positioning  
M18 ; disable steppers  
